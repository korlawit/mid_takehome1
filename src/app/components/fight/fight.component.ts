import { Component, OnInit } from '@angular/core';
import { fight } from 'src/app/fight';
import { FormBuilder,FormGroup,NgForm } from '@angular/forms';
import { FightService } from 'src/app/share/fight.service';

@Component({
  selector: 'app-fight',
  templateUrl: './fight.component.html',
  styleUrls: ['./fight.component.css']
})
export class FightComponent implements OnInit {

  fight: fight;
  fights: Array<fight>=[]

  todayDate:Date = new Date();

  fightForm !: FormGroup;

  constructor(private fb: FormBuilder,private pageService:FightService) {
    var day = new Date();
    var change = new Date(new Intl.DateTimeFormat('th-TH').format(day))
    this.fight = new fight('','','','',0,day,0,0,day)
   }

  ngOnInit(): void {
    this.fightForm=this.fb.group({
      fullname:[''],
      from:[''],
      to:[''],
      type:[''],
      adults:[''],
      departure:[''],
      children:[''],
      infants:[''],
      arrival:['']


    });
    this.getFriendPages();
  }

  getFriendPages(){
    this.fights=this.pageService.getFriends();
  }

  onSubmit(f:FormGroup): void{







    let form_record = new fight(f.get('fullname')?.value,f.get('from')?.value,f.get('to')?.value,f.get('type')?.value
    ,f.get('adults')?.value,f.get('departure')?.value,f.get('children')?.value,f.get('infants')?.value,f.get('arrival')?.value);
    this.pageService.addFriend(form_record)






  }

}
