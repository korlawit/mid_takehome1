import { Injectable } from '@angular/core';
import { fight } from '../fight';
import { Mockfight } from './mockfight';

@Injectable({
  providedIn: 'root'
})
export class FightService {
  fights:fight[]=[];

  constructor() {
    this.fights=Mockfight.mfight;
   }

   getFriends():fight[]{
     return this.fights;
   }

   addFriend(f:fight): void{
     this.fights.push(f);
   }
}
